class Customer implements Comparable<Customer> {

    public int CustomerAgre;
    public String Name;
    public String Surname;

    public Customer(){

    }

    public Customer(int CustomerAgre, String name, String Surname) {
        this.CustomerAgre = CustomerAgre;
        this.Name = name;
        this.Surname = Surname;
    }

    @Override
    public int compareTo(Customer o) {
        return o.CustomerAgre > this.CustomerAgre ? 1 : -1;
    }

    @Override
    public String toString() {
        return "Customer Age: " + this.CustomerAgre + ", CustomerName: " + this.Name + ", Customer Surname: " + Surname;
    }

}

